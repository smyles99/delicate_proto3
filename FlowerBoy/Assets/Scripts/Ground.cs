﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite infected;
    public bool shovelPickedUp;

    public GameObject seed;

    private void Start()
    {
        shovelPickedUp = true;
        seed.SetActive(false);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
      spriteRenderer.sprite = infected;
      gameObject.tag = ("Planted");

        if (gameObject.CompareTag("Planted"))
        {
            if (Input.GetKey(KeyCode.F))
            {
                seed.SetActive(true);
                Debug.Log("hi");
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (gameObject.CompareTag("Shovel"))
        {
            if (Input.GetKey(KeyCode.F))
            {
               
                shovelPickedUp = false;
                gameObject.SetActive(false);
                Debug.Log("hi");

            }
            

        }
        
    }

    public void Update()
    {
        
        {
          
        }
    }
}